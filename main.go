package main

import (
	"flag"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/xanzy/go-gitlab"
)

const (
	searchDaysDefault = 7
	projectLink       = "[1on1-report-bot](https://gitlab.com/mnielsen/1on1-report-bot)"
)

type timeframe struct {
	after, before gitlab.ISOTime
	days          int
}

type report struct {
	timeframe timeframe
	content   strings.Builder
}

func newISOTime(date string) gitlab.ISOTime {
	var result gitlab.ISOTime
	raw := fmt.Sprintf("\"%s\"", date)

	if err := result.UnmarshalJSON([]byte(raw)); err != nil {
		log.Fatalf("unable to unmarshal time: %v", err)
	}

	return result
}

func newTimeframe(searchDays int) timeframe {
	afterDays := (-searchDays - 1)
	beforeDays := time.Now().AddDate(0, 0, 1).Format("2006-01-02")

	after := newISOTime(time.Now().AddDate(0, 0, afterDays).Format("2006-01-02"))
	before := newISOTime(beforeDays)

	log.Printf("searching after %s and before %s\n", after, before)

	return timeframe{
		before: before,
		after:  after,
		days:   searchDays,
	}
}

func (r *report) run(gl *gitlab.Client) error {
	mrs, err := listEventsForTargetType(gl, r.timeframe, gitlab.MergeRequestEventTargetType)
	if err != nil {
		return err
	}

	issues, err := listEventsForTargetType(gl, r.timeframe, gitlab.IssueEventTargetType)
	if err != nil {
		return err
	}

	var allEvents []*gitlab.ContributionEvent
	allEvents = append(allEvents, mrs...)
	allEvents = append(allEvents, issues...)

	r.content.WriteString(createReport(gl, allEvents, r.timeframe))

	return nil
}

func (r *report) createSnippet(gl *gitlab.Client, userID int) error {
	year, month, day := time.Now().Date()
	title := fmt.Sprintf("1 on 1: %s %d, %d", month.String(), day, year)
	userName := getUserName(gl, &userID)
	description := fmt.Sprintf("Automated work report for %s generated via %s", userName, projectLink)

	snippetOpts := &gitlab.CreateSnippetOptions{
		Title:       gitlab.String(title),
		FileName:    gitlab.String("1on1.md"),
		Description: gitlab.String(description),
		Content:     gitlab.String(r.content.String()),
		Visibility:  gitlab.Visibility(gitlab.PrivateVisibility),
	}

	snippet, _, err := gl.Snippets.CreateSnippet(snippetOpts)
	if err != nil {
		return err
	}

	log.Println("created snippet:", snippet.WebURL)
	return nil
}

func main() {
	var userID = flag.Int("userID", 0, "GitLab user ID from profile page")
	var token = flag.String("token", "", "GitLab access token with API scope")
	var searchDays = flag.Int("days", searchDaysDefault, "number of days back to search")
	var createSnippet = flag.Bool("createSnippet", true, "create a snippet with report output")
	flag.Parse()

	if *userID == 0 {
		log.Fatal("must provide a user ID via `-userID`")
	}

	if *token == "" {
		log.Fatal("must provide a token via `-token`")
	}

	gl, err := gitlab.NewClient(*token)
	if err != nil {
		log.Fatalf("unable to create client: %v", err)
	}

	var report report
	report.timeframe = newTimeframe(*searchDays)
	err = report.run(gl)
	if err != nil {
		log.Fatal(err)
	}

	if *createSnippet {
		err = report.createSnippet(gl, *userID)
		if err != nil {
			log.Fatalf("failed to create snippet for report (content below)\n\n%s", report.content.String())
		}
	} else {
		log.Printf("Report:\n\n%s", report.content.String())
	}
}
