module gitlab.com/mnielsen/1on1-report-bot/ci

go 1.19

require gitlab.com/mnielsen/gitlab-ci-go v0.0.3

require gopkg.in/yaml.v3 v3.0.1 // indirect
