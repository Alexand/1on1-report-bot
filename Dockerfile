FROM golang:1.19 AS build
WORKDIR /build
COPY go.mod go.sum ./
RUN go mod download
COPY main.go helpers.go ./
RUN CGO_ENABLED=0 GOOS=linux go build -o 1on1bot ./...


FROM alpine:3.9 AS release
RUN apk add ca-certificates
COPY --from=build /build/1on1bot /1on1bot
ENTRYPOINT ["/1on1bot"]
