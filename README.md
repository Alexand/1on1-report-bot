# 1 on 1 Report Bot

The 1 on 1 report bot generates a report for use in 1 on 1 meetings with a manager.

## Example

[Example snippet](https://gitlab.com/-/snippets/2501715)

## Usage

1. Collect your user ID from your [GitLab.com profile](https://gitlab.com/-/profile) (located under `Main settings` next to your full name).
1. Create an API token with the `api` scope via your [Personal Access Tokens page](https://gitlab.com/-/profile/personal_access_tokens).
1. Run `docker run registry.gitlab.com/mnielsen/1on1-report-bot:latest -token=<your token> -userID=<your user ID> [-days=7] [-createSnippet=true]`
1. Collect the snippet URL that's printed out and enter it in your browser.
1. (Optional) Share the snippet with your manager (the snippets are private by defaut).

## TODO

- [x] Create proof of concept
- [x] Build and test in GitLab CI
- [ ] Add tests
- [ ] Upload binary to Project Releases page
