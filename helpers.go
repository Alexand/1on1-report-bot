package main

import (
	"fmt"
	"log"
	"strings"

	"github.com/elliotchance/pie/v2"
	"github.com/xanzy/go-gitlab"
)

func listEventsForTargetType(gl *gitlab.Client, tf timeframe, targetType gitlab.EventTargetTypeValue) ([]*gitlab.ContributionEvent, error) {
	opts := &gitlab.ListContributionEventsOptions{
		TargetType: &targetType,
		ListOptions: gitlab.ListOptions{
			Page:    1,
			PerPage: 10,
		},
		After:  &tf.after,
		Before: &tf.before,
	}

	var events []*gitlab.ContributionEvent

	for {
		page_events, resp, err := gl.Events.ListCurrentUserContributionEvents(opts)
		if err != nil {
			return events, err
		}

		events = append(events, page_events...)

		if resp.NextPage == 0 {
			break
		}

		opts.ListOptions.Page = resp.NextPage
	}

	if targetType == gitlab.MergeRequestEventTargetType {
		events = renameAcceptedEvents(events)
		events = combineApprovedMerged(events)
	}

	return events, nil
}

func renameAcceptedEvents(events []*gitlab.ContributionEvent) []*gitlab.ContributionEvent {
	return pie.Each(events, func(e *gitlab.ContributionEvent) {
		if e.ActionName == "accepted" {
			e.ActionName = "merged"
		}
	})
}

func combineApprovedMerged(events []*gitlab.ContributionEvent) []*gitlab.ContributionEvent {
	for i := 0; i < len(events); i++ {
		for j := 1; j < len(events)-1; j++ {
			if events[i].TargetIID == events[j].TargetIID {
				if events[i].ActionName != events[j].ActionName {
					if events[i].ActionName == "merged" {
						events = remove(events, j)
					}
				}
			}
		}
	}

	return events
}

func remove(slice []*gitlab.ContributionEvent, s int) []*gitlab.ContributionEvent {
	return append(slice[:s], slice[s+1:]...)
}

func createReport(gl *gitlab.Client, events []*gitlab.ContributionEvent, tf timeframe) string {
	var result strings.Builder

	result.WriteString(fmt.Sprintf("# Activity report after %s and before %s (past %d days)\n", tf.after, tf.before, tf.days))

	for _, pid := range getUniqueProjectIDs(events) {
		project := getProject(gl, pid)
		result.WriteString(fmt.Sprintf("\n## %s\n", project.Name))

		printIssueEntries(gl, events, pid, &result)
		printMergeRequestEntries(gl, events, pid, &result)
	}

	return result.String()
}

func printIssueEntries(gl *gitlab.Client, events []*gitlab.ContributionEvent, pid int, result *strings.Builder) {
	issueEvents := getEventsByTargetTypeForProject(events, "Issue", pid)
	if len(issueEvents) > 0 {
		result.WriteString("\n### Issues\n")

		for _, event := range issueEvents {
			issue := getIssue(gl, event.ProjectID, event.TargetIID)

			entryResult := fmt.Sprintf("  - %s [%s](%s)\n", event.ActionName, issue.Title, issue.WebURL)
			if issue.Confidential {
				entryResult = fmt.Sprintf("  - %s [(confidential)](%s)\n", event.ActionName, issue.WebURL)
			}

			result.WriteString(entryResult)
		}
	}
}

func printMergeRequestEntries(gl *gitlab.Client, events []*gitlab.ContributionEvent, pid int, result *strings.Builder) {
	mergeRequestEvents := getEventsByTargetTypeForProject(events, "MergeRequest", pid)
	if len(mergeRequestEvents) > 0 {
		result.WriteString("\n### Merge requests\n")

		for _, event := range mergeRequestEvents {
			mr := getMergeRequest(gl, event.ProjectID, event.TargetIID)

			entryResult := fmt.Sprintf("  - %s [%s](%s)\n", event.ActionName, mr.Title, mr.WebURL)
			result.WriteString(entryResult)
		}
	}
}

func getEventsByTargetTypeForProject(events []*gitlab.ContributionEvent, targetType string, pid int) []*gitlab.ContributionEvent {
	result := pie.Filter(events, func(event *gitlab.ContributionEvent) bool {
		return event.TargetType == targetType && event.ProjectID == pid
	})

	result = pie.SortUsing(result, func(a, b *gitlab.ContributionEvent) bool {
		return strings.Compare(a.TargetTitle, b.TargetTitle) == -1
	})

	return result
}

func getUniqueProjectIDs(events []*gitlab.ContributionEvent) []int {
	pids := make([]int, len(events))
	for i := range events {
		pids[i] = events[i].ProjectID
	}

	return pie.Unique(pids)
}

func getProject(gl *gitlab.Client, pid int) *gitlab.Project {
	project, _, err := gl.Projects.GetProject(pid, nil)
	if err != nil {
		log.Fatalf("unable to get project title by ID: %v", err)
	}

	return project
}

func getMergeRequest(gl *gitlab.Client, pid interface{}, mrIID int) *gitlab.MergeRequest {
	mr, _, err := gl.MergeRequests.GetMergeRequest(pid, mrIID, nil)
	if err != nil {
		log.Fatalf("unable to get MR by project ID %v and MR IID %d", pid, mrIID)
	}

	return mr
}

func getIssue(gl *gitlab.Client, pid, id int) *gitlab.Issue {
	issue, _, err := gl.Issues.GetIssue(pid, id, nil)
	if err != nil {
		log.Fatalf("unable to get Issue by project ID %d and issue ID %d", pid, id)
	}

	return issue
}

func getUserName(gl *gitlab.Client, userID *int) string {
	user, _, err := gl.Users.GetUser(*userID)
	if err != nil {
		log.Fatalf("unable to get user by ID %d, %v", userID, err)
	}

	return user.Name
}
